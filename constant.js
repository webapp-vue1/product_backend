/* eslint-disable eol-last */
/* eslint-disable indent */
const ROLE = {
    ADMIN: 'ADMIN',
    LOCAL_ADMID: 'LOCAL_ADMIN',
    USER: 'USER'
}

module.exports = {
    ROLE
}