/* eslint-disable no-undef */
/* eslint-disable no-unused-vars */
/* eslint-disable eol-last */
/* eslint-disable space-before-function-paren */
/* eslint-disable indent */
const express = require('express')
const router = express.Router()
const Product = require('../models/Product')

const getProducts = async function(req, res, next) {
    try {
        const products = await Product.find({}).exec()
        res.status(200).json(products)
    } catch (err) {
        return res.status(500).send({
            message: err.message
        })
    }
}
const getProduct = async function(req, res, next) {
    const id = (req.params.id)
    console.log(id)
    try {
        const product = await Product.findById(id).exec()
        if (product === null) {
            return res.status(404).json({
                message: 'Product not found!!'
            })
        }
        res.json(product)
    } catch (err) {
        return res.status(404).json({
            message: err.message
        })
    }
}

const addProducts = async function(req, res, next) {
    const newProduct = new Product({
        name: req.body.name,
        price: parseFloat(req.body.price)
    })
    try {
        await newProduct.save()
        res.status(201).json(newProduct)
    } catch (err) {
        return res.status(500).send({
            message: err.message
        })
    }
}
const updateProduct = function(req, res, next) {
    const productId = parseInt(req.params.id)
    const product = {
        id: parseInt(req.body.id),
        name: req.body.name,
        price: parseFloat(req.body.price)
    }
    const index = products.findIndex(function(item) {
        return item.id === parseInt(req.params.id)
    })
    if (index >= 0) {
        products[index] = product
        res.json(products[index])
    } else {
        res.status(404).json({
            Code: 404,
            msg: 'No Products id ' + req.params.id
        })
    }
}

const deleteProduct = async function(req, res, next) {
    const productId = req.params.id
    try {
        await Product.findByIdAndDelete(productId)
        return res.status(200).send()
    } catch (err) {
        return res.status(404).send({ message: err.message })
    }
}
router.get('/', getProducts) // GET Products
router.get('/:id', getProduct) // GET One Product
router.post('/', addProducts) // Add New Product
router.put('/:id', updateProduct) // Add New Product
router.delete('/:id', deleteProduct)
module.exports = router